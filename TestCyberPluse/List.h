#pragma once
#include <cassert>
#include "Node.h"

class List
{
public:
	List();
	~List();

	Node* getFirst();
	void append(int value);

	void swap(Node* l, Node* r);

	Node* operator[] (const int index);
	friend std::ostream& operator<<(std::ostream& out, const List& list);

private:
	Node* _head;
	
};


#pragma once

#include <iostream>

class Node {
public:
    Node(int value);
    ~Node();

    int& getValue();
    void setValue(int& newValue);

    Node* getNext();
    void setNext(Node* node);

    Node* getPrev();
    void setPrev(Node* node);

    friend std::ostream& operator<<(std::ostream& out, const Node& node);

private:
    int _value;
    Node* _next;
    Node* _prev;

};

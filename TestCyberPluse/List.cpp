#include "List.h"

List::List()
{
	_head = nullptr;
}

List::~List()
{
	delete _head;
}

Node* List::getFirst()
{
	return _head;
}

void List::append(int value)
{	
	Node* newNode = new Node(value);
	if (_head) {
		Node* lastNode = _head;
		while (lastNode->getNext()) {
			lastNode = lastNode->getNext();
		}
		lastNode->setNext(newNode);
		newNode->setPrev(lastNode);
	}
	else {
		_head = newNode;
	}
}



void List::swap(Node* l, Node* r)
{

	Node* neighbor = l->getPrev();
	if (neighbor)
		neighbor->setNext(r);

	neighbor = l->getNext();
	if (neighbor)
		neighbor->setPrev(r);

	neighbor = r->getPrev();
	if (neighbor)
		neighbor->setNext(l);

	neighbor = r->getNext();
	if (neighbor)
		neighbor->setPrev(l);


	Node* temp = l->getNext();
	l->setNext(r->getNext());
	r->setNext(temp);

	temp = l->getPrev();
	l->setPrev(r->getPrev());
	r->setPrev(temp);
}

Node* List::operator[](const int index)
{
	Node* currentNode = _head;
	int i = 0;
	while (currentNode && i < index)
	{
		currentNode = currentNode->getNext();
		++i;
	}

	assert(currentNode != nullptr);
	 
	return currentNode;
}

std::ostream& operator<<(std::ostream& out, const List& list)
{
	Node* currentNode = list._head;
	bool isFistValue = true;

	while (currentNode) {
		if (isFistValue) {
			isFistValue = false;
		}
		else {
			out << ", ";
		}
		out << *currentNode;
		currentNode = currentNode->getNext();
	}
	
	return out;
}

#include "Node.h"

Node::Node(int value) 
{
	setValue(value);
	setNext(nullptr);
	setPrev(nullptr);
}

Node::~Node()
{
	if (_next)
		delete _next;
}

int& Node::getValue()
{
	return _value;
}

void Node::setValue(int& newValue)
{
	_value = newValue;
}

Node* Node::getNext()
{
	return _next;
}

void Node::setNext(Node* node)
{
	_next = node;
}

Node* Node::getPrev()
{
	return _prev;
}

void Node::setPrev(Node* node)
{
	_prev = node;
}

std::ostream& operator<<(std::ostream& out, const Node& node)
{
	out << node._value;
	return out;
}

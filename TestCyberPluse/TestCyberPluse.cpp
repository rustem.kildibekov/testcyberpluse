#include <iostream>
#include "List.h"

int main()
{
    List list;
    for (int i = 0; i < 10; i++)
        list.append(i);

    std::cout << list << std::endl;
    list.swap(list[3], list[5]);
    std::cout << list << std::endl;

    do
    {
        std::cout << '\n' << "Press a key to continue...";
    } while (std::cin.get() != '\n');

    return 0;

}

